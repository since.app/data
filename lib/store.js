// Package imports
const mongoose = require('mongoose')
const Store = require('since.store')

// Application imports
const { db } = require('../.env')

// Module exports a connection to the timestamp database
module.exports = new Store(db, mongoose)
