// Package imports
const mongoose = require('mongoose')
const { str: { uuidv4 } } = require('since.util')

// Application imports
const Timestamp = require('./Timestamp')

// Aliases
const Relation = mongoose.Schema.Types.ObjectId

// Definitions
const name = 'Tag'
const schema = new mongoose.Schema({
  _id: {
    type: String,
    default: uuidv4
  },
  // User.id from since.auth
  user: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  related: {
    type: Array
  },
  timestamps: [{
    type: Relation,
    ref: Timestamp.class
  }]
}, {
  timestamps: {
    createdAt: 'created',
    updatedAt: 'updated'
  }
})

// Module exports the Mongoose model
module.exports = mongoose.model(name, schema)

// Module also exports the name of the model for late static binding
module.exports.class = name

// Module also exports the raw Mongoose schema
module.exports.schema = schema
