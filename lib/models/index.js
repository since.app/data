// Application imports
const Tag = require('./Tag')
const Timestamp = require('./Timestamp')

// Module exports Mongoose models
module.exports = {
  Tag,
  Timestamp
}
