// Application imports
const env = require('../.env')

// Module exports a reference to our dotenv
module.exports = env
