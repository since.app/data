/**
 * Home route configuration.
 *
 * @type {Array}
 */
module.exports = {
  url: '/',
  method: 'GET',
  handler: async ({ user }) => {
    return `@todo redirect ${user.name}`
  }
}
