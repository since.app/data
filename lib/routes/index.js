// Application imports
const routes = [
  require('./home')
]

// Module exports a flattened array of route configurations.
module.exports = routes.reduce((a, v) => a.concat(v), [])
