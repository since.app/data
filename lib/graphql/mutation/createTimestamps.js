// Application imports
const createModels = require('./createModels')
const { Timestamp } = require('lib/models')

/**
 * Creates timestamps in our database.
 *
 * @function createTimestamp
 * @param   {Object}          _        ...
 * @param   {Object}          args     Query variables
 * @param   {Object}          response Request response
 * @returns {Promise<Object>}          Updated timestamp
 */
module.exports = function createTimestamps () {
  return createModels(Timestamp, ...arguments)
}
