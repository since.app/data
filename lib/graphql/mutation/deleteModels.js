// Package imports
const { belongs, exists } = require('since.store').validate

// Application imports
const store = require('lib/store')

/**
 * Deletes models from our database.
 *
 * @async
 * @function deleteModels
 * @param   {Object}          _          ...
 * @param   {Object}          args       Query variables
 * @param   {Object}          args.input User input
 * @returns {Promise<Object>}            Number of deleted timestamps
 */
module.exports = async function deleteModels (model, _, { input }, { reply }) {
  // Getting the user UUID from our validated JWT
  const user = reply.request.user.sub

  // Make sure the timestamps the user is trying to update exists and belongs to them
  await store.validate(model, input, [exists(), belongs(user)])

  // Return the removed documents
  return store.remove(model, input)
}
