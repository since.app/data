// Package imports
const { belongs, exists } = require('since.store').validate

// Application imports
const store = require('lib/store')

/**
 * Updates models in our database.
 *
 * @async
 * @function updateTimestamp
 * @param   {Model}           model      Mongoose static model
 * @param   {Object}          _          ...
 * @param   {Object}          args       Query variables
 * @param   {Object}          args.input User input
 * @returns {Promise<Object>}            Updated timestamp
 */
module.exports = async function updateModels (model, _, { input }, { reply }) {
  // Getting the user UUID from our validated JWT
  const user = reply.request.user.sub

  // Make sure the timestamps the user is trying to update exists and belongs to them
  await store.validate(model, input, [belongs(user), exists()])

  // Return the updated timestamps
  return store.update(model, input)
}
