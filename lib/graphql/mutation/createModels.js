// Package imports
const { relate, validate: { exists } } = require('since.store')

// Application imports
const store = require('lib/store')

/**
 * Creates models in our database.
 *
 * @async
 * @function createModels
 * @param   {Object}          _          ...
 * @param   {Object}          args       Query variables
 * @param   {Object}          args.input User input
 * @returns {Promise<Object>}            Created timestamp
 */
module.exports = async function createModels (model, _, { input }, { reply }) {
  // Getting the user UUID from our validated JWT
  const user = reply.request.user.sub

  // Prevent duplicate UUIDs
  await store.validate(model, input, exists(true))

  // Save data to MongoDB
  return store.add(model, relate(input, { user }))
}
