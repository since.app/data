// Application imports
const deleteModels = require('./deleteModels')
const { Timestamp } = require('lib/models')

/**
 * Deletes timestamps from our database.
 *
 * @function deleteTimestamp
 * @param   {Object}          _        ...
 * @param   {Object}          args     Query variables
 * @param   {Object}          response Request response
 * @returns {Promise<Object>}          Updated timestamp
 */
module.exports = function deleteTimestamps () {
  return deleteModels(Timestamp, ...arguments)
}
