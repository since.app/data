// Application imports
const deleteModels = require('./deleteModels')
const { Tag } = require('lib/models')

/**
 * Deletes a tag from our database.
 *
 * @function deleteTags
 * @param   {Object}          _        ...
 * @param   {Object}          args     Query variables
 * @param   {Object}          response Request response
 * @returns {Promise<Object>}          Number of deleted tags
 */
module.exports = function deleteTags () {
  return deleteModels(Tag, ...arguments)
}
