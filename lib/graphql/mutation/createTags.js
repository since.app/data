// Application imports
const createModels = require('./createModels')
const { Tag } = require('lib/models')

/**
 * Creates a tag in our database.
 *
 * @function createTags
 * @param   {Object}          _        ...
 * @param   {Object}          args     Query variables
 * @param   {Object}          response Request response
 * @returns {Promise<Object>}          Created tags
 */
module.exports = function createTags () {
  return createModels(Tag, ...arguments)
}
