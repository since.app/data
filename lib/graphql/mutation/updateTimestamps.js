// Application imports
const updateModels = require('./updateModels')
const { Timestamp } = require('lib/models')

/**
 * Updates timestamps in our database.
 *
 * @function updateTimestamp
 * @param   {Object}          _        ...
 * @param   {Object}          args     Query variables
 * @param   {Object}          response Request response
 * @returns {Promise<Object>}          Updated timestamp
 */
module.exports = function updateTimestamps () {
  return updateModels(Timestamp, ...arguments)
}
