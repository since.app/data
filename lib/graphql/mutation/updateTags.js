// Application imports
const updateModels = require('./updateModels')
const { Tag } = require('lib/models')

/**
 * Updates a tag in our database.
 *
 * @function updateTag
 * @param   {Object}          _        ...
 * @param   {Object}          args     Query variables
 * @param   {Object}          response Request response
 * @returns {Promise<Object>}          Updated tag
 */
module.exports = function updateTags () {
  return updateModels(Tag, ...arguments)
}
