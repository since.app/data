// Application imports
const createTimestamp = require('./createTimestamps')
const deleteTimestamp = require('./deleteTimestamps')
const updateTimestamp = require('./updateTimestamps')

// Module exports default object containing all Fastify GraphQL mutation resolvers
module.exports = {
  createTimestamp,
  createTimestamps: createTimestamp,
  deleteTimestamp,
  deleteTimestamps: deleteTimestamp,
  updateTimestamp,
  updateTimestamps: updateTimestamp
}
