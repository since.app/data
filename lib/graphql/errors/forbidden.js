module.exports = class Forbidden extends Error {
  constructor (model, input) {
    super(`Invalid ${model.class}: ${input.id} forbidden`)
  }
}
