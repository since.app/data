// Module imports
const BadRequest = require('./bad-request')
const Forbidden = require('./forbidden')
const NotFound = require('./not-found')

// Module exports HTTP status code like errors
module.exports = {
  BadRequest,
  Forbidden,
  NotFound
}
