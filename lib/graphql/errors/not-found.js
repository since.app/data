module.exports = class NotFound extends Error {
  constructor (model, input) {
    super(`Invalid ${model.class}: ${input.id} not found`)
  }
}
