module.exports = class BadRequest extends Error {
  constructor (model, message) {
    super(model.class + ' bad request' + (message ? ': ' + message : ''))
  }
}
