// Package imports
const { Date } = require('since.gql').resolvers

// Application imports
const Mutation = require('./mutation')
const Query = require('./query')

// Module exports resolvers for Fastify GraphQL
module.exports = {
  Date,
  Mutation,
  Query
}
