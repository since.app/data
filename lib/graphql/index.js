// Application imports
const resolvers = require('./resolvers')
const schema = require('./schema')

// Module exports references to create a FastifyJS GraphQL endpoint
module.exports = {
  resolvers,
  schema
}
