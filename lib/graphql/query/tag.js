// Application imports
const store = require('lib/store')
const { Tag } = require('lib/models')

/**
 * Finds a tag by UUID.
 *
 * @async
 * @function tag
 * @param   {Object}          _       ...
 * @param   {Object}          args    Query variables
 * @param   {String}          args.id Tag ID
 * @returns {Promise<Object>}         Tag object
 */
module.exports = async function tag (_, { input }) {
  return store.one(Tag, input)
}
