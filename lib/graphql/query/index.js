// Application imports
// const tag = require('./tag')
// const tags = require('./tags')
const timestamp = require('./timestamp')
const timestamps = require('./timestamps')

// Module exports default object containing all Fastify GraphQL query resolvers
module.exports = {
  // tag,
  // tags,
  timestamp,
  timestamps
}
