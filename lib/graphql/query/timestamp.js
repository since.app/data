// Application imports
const store = require('lib/store')
const { Timestamp } = require('lib/models')

/**
 * Finds a timestamp by UUID.
 *
 * @async
 * @function timestamp
 * @param   {Object}          _       ...
 * @param   {Object}          args    Query variables
 * @param   {String}          args.id Timestamp ID
 * @returns {Promise<Object>}         Timestamp object
 */
module.exports = async function timestamp (_, { id }) {
  return store.one(Timestamp, id)
}
