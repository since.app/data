// Application imports
const store = require('lib/store')
const { Tag } = require('lib/models')

/**
 * Finds multiple tags by filter.
 *
 * @async
 * @function tags
 * @param   {Object}            _           ...
 * @param   {Object}            args        Query variables
 * @param   {String}            args.filter Optional filtering
 * @returns {Promise<Object[]>}             Tags matching filter
 */
module.exports = async function tags (_, { filter }) {
  return store.filter(Tag, filter)
}
