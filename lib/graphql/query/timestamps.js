// Application imports
const models = require('./models')
const { Timestamp } = require('lib/models')

/**
 * Finds multiple timestamps by filter.
 *
 * @async
 * @function timestamps
 * @param   {Object}            _           ...
 * @param   {Object}            args        Query variables
 * @param   {String}            args.filter Optional filter
 * @returns {Promise<Object[]>}             Timestamps matching filter
 */
module.exports = async function timestamps () {
  return models(Timestamp, ...arguments)
}
