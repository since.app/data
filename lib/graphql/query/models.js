// Package imports
const { relate } = require('since.store')

// Application imports
const store = require('lib/store')

/**
 * Finds multiple models by filter.
 *
 * @async
 * @function timestamps
 * @param   {Object}            _           ...
 * @param   {Object}            args        Query variables
 * @param   {String}            args.filter Optional filter
 * @returns {Promise<Object[]>}             Timestamps matching filter
 */
module.exports = async function models (model, _, { filter }, { reply }) {
  // Getting the user UUID from our validated JWT
  const user = reply.request.user.sub

  // Always filter models by user
  return store.filter(model, relate(filter || {}, { user }))
}
