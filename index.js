// Package imports
const server = require('since.fast')
const graphql = require('since.gql')
const { jwt, router } = server

// Application imports
const env = require('./.env')
const routes = require('./lib/routes')
const { resolvers, schema } = require('./lib/graphql')

// Run server
server([jwt(), router(routes), graphql(schema, resolvers)], env)
